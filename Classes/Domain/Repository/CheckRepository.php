<?php

declare(strict_types=1);

namespace NERDOST\InfodiensteCheck\Domain\Repository;


/**
 * This file is part of the "t3infodienste Check" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Lukas Kleist <l.kleist@nerdost.net>, Nerdost GmbH
 */

/**
 * The repository for Checks
 */
class CheckRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
