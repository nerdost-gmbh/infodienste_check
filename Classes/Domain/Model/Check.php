<?php

declare(strict_types=1);

namespace NERDOST\InfodiensteCheck\Domain\Model;


/**
 * This file is part of the "t3infodienste Check" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Lukas Kleist <l.kleist@nerdost.net>, Nerdost GmbH
 */

/**
 * Check
 */
class Check extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
}
