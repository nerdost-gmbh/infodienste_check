<?php

declare(strict_types=1);

namespace NERDOST\InfodiensteCheck\Controller;


use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This file is part of the "t3infodienste Check" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Lukas Kleist <l.kleist@nerdost.net>, Nerdost GmbH
 */

/**
 * CheckController
 */
class CheckController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    const REQUIREMENTS = [
        'max_execution_time' => 1800,
        'max_input_time' => 1800,
        'memory_limit' => '256M',
    ];

    const MAILTO = [
        'receiver' => 'anfrage@t3-infodienste.de',
        'subject' => 'Anfrage zur t3infodienste Extension'
    ];

    /**
     * checkRepository
     *
     * @var \NERDOST\InfodiensteCheck\Domain\Repository\CheckRepository
     */
    protected $checkRepository = null;

    /**
     * @param \NERDOST\InfodiensteCheck\Domain\Repository\CheckRepository $checkRepository
     */
    public function injectCheckRepository(\NERDOST\InfodiensteCheck\Domain\Repository\CheckRepository $checkRepository)
    {
        $this->checkRepository = $checkRepository;
    }

    /**
     * action new
     */
    public function newAction()
    {
        $typo3Version = new \TYPO3\CMS\Core\Information\Typo3Version();
        $coreVersion = $typo3Version->getVersion();

        $bootstrapVersion = 3;
        if (version_compare($coreVersion, '11', '>=') == true) {
            $bootstrapVersion = 5;
        }

        $this->view->assignMultiple([
            'mailto' => self::MAILTO,
            'bootstrap' => $bootstrapVersion,
            'layout' => 'Default',
        ]);

        $versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
        if ($versionInformation->getMajorVersion() >= 11) {
            $this->view->assign('layout', 'Default-v12');

            $moduleTemplateFactory = GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Template\ModuleTemplateFactory::class);
            $moduleTemplate = $moduleTemplateFactory->create($this->request);
            $moduleTemplate->setContent($this->view->render());
            return $this->htmlResponse($moduleTemplate->renderContent());
        }
    }

    /**
     * action create
     * @param array $newCheck
     */
    public function createAction($newCheck = [])
    {
        $checkup = $this->systemCheck();

        $this->view->assignMultiple([
            'checkup' => $checkup,
            'newCheck' => $newCheck,
            'required' => self::REQUIREMENTS,
            'mailto' => self::MAILTO,
            'layout' => 'Default',
        ]);

        $versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
        if ($versionInformation->getMajorVersion() >= 11) {
            $this->view->assign('layout', 'Default-v12');

            $moduleTemplateFactory = GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Template\ModuleTemplateFactory::class);
            $moduleTemplate = $moduleTemplateFactory->create($this->request);
            $moduleTemplate->setContent($this->view->render());
            return $this->htmlResponse($moduleTemplate->renderContent());
        }
    }


    protected function systemCheck()
    {
        $typo3Version = new \TYPO3\CMS\Core\Information\Typo3Version();
        $coreVersion = $typo3Version->getVersion();

        $result = [
            'core' => $coreVersion,
            'soap' => extension_loaded('soap'),
            'curl' => extension_loaded('curl'),
            'mysqldump' => false,
            'mysqlImport' => false,
            'overallResult' => true
        ];


        $maxExecTime = ini_get('max_execution_time');
        $maxExecTimeProoved = self::REQUIREMENTS['max_execution_time'] < (int) $maxExecTime;
        $memLimit = ini_get('memory_limit');
        $memLimitProoved = (int) self::REQUIREMENTS['memory_limit'] < (int) $memLimit;
        $maxInputTime = ini_get('max_input_time');
        $maxInputTimeProoved = (self::REQUIREMENTS['max_input_time'] < (int) $maxInputTime || (int) $maxInputTime == -1);

        $result['phpSettings'] = [
            'max_execution_time' => [
                'prooved' => $maxExecTimeProoved,
                'value' => $maxExecTime
            ],
            'memory_limit' => [
                'prooved' => $memLimitProoved,
                'value' => $memLimit
            ],
            'max_input_time' => [
                'prooved' => $maxInputTimeProoved,
                'value' => $maxInputTime
            ],
        ];

        if(!$maxExecTimeProoved || !$memLimitProoved
            || !$maxInputTimeProoved || !$result['soap'] || !$result['curl']) {
            $result['overallResult'] = false;
        }


        $defaultDB = $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'];

        if (isset($defaultDB) === true && empty($defaultDB) === false) {
            $dbname = $defaultDB['dbname'];
            $user = $defaultDB['user'];
            $password = $defaultDB['password'];
            $host = $defaultDB['host'];

            $backupPath = \TYPO3\CMS\Core\Core\Environment::getPublicPath() . '/fileadmin';
            $backupFile = 'infodiensteCheck.sql';
            $backupFilePath = $backupPath . '/' . $backupFile;
            $compressedFilePath = $backupFilePath.'.gz';

            $mysqlBasic = '--user=' . $user .
                ' --password=' . $password .
                ' --host=' . $host;

            $mysqldump = 'mysqldump ' . $mysqlBasic;


            $checkTable = 'tx_infodienstecheck_domain_model_check';
            $mysqldump .= ' ' . $dbname . ' '. $checkTable .' | gzip -9 > ' . $compressedFilePath;
            exec($mysqldump, $exportExecOutput, $exportReturn);

            $result['mysqldump'] = ($exportReturn === 0 && filesize($compressedFilePath) > 500);

            if($result['mysqldump'] === false) {
                $result['overallResult'] = false;
                return $result;
            }

            /** @var \TYPO3\CMS\Core\Database\Connection $connection */
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable($checkTable);

            $connection->truncate($checkTable);

            $mysql = 'gunzip < '.$compressedFilePath.
                ' | mysql --user='.$user.
                ' --password='.$password.
                ' --host='.$host.
                ' --database='.$dbname;

            exec($mysql, $importExecOutput, $importReturn);

            if ($connection->select(['*'], $checkTable)->rowCount() > 0) {
                $result['mysqlImport'] = true;
            } else {
                $result['overallResult'] = false;
            }

            if (file_exists($compressedFilePath)) {
                unlink($compressedFilePath);
            }
        }

        return $result;
    }
}
