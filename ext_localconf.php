<?php
defined('TYPO3') || die();

$versionInformation = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(TYPO3\CMS\Core\Information\Typo3Version::class);
if ($versionInformation->getMajorVersion() < 12) {
    $extKey = 'infodienste_check';
    $GLOBALS['TYPO3_CONF_VARS']['BE']['stylesheets'][$extKey] = 'EXT:' . $extKey . '/Resources/Public/Css/checkupModule.css';
}
