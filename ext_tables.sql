#
# Table structure for table 'tx_infodienstecheck_domain_model_check'
#
CREATE TABLE tx_infodienstecheck_domain_model_check (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,

    title varchar(20) NOT NULL default '',
    text text,

    PRIMARY KEY (uid),
    KEY parent (pid),
);