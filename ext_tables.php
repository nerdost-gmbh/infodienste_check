<?php
defined('TYPO3') || die();

(static function() {

    $versionInformation = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(TYPO3\CMS\Core\Information\Typo3Version::class);
    if ($versionInformation->getMajorVersion() < 12) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'Nerdost.InfodiensteCheck',
            'tools', // Make module a submodule of 'web'
            'infocheck', // Submodule key
            'bottom', // Position
            [
                \NERDOST\InfodiensteCheck\Controller\CheckController::class => 'new,create,list,index',
            ],
            [
                'access' => 'admin',
                'icon'   => 'EXT:infodienste_check/Resources/Public/Icons/t3i.svg',
                'labels' => 'LLL:EXT:infodienste_check/Resources/Private/Language/locallang_infocheck.xlf',
            ]
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
            'tx_infodienstecheck_domain_model_check',
            'EXT:infodienste_check/Resources/Private/Language/locallang_csh_tx_infodienstecheck_domain_model_check.xlf'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_infodienstecheck_domain_model_check');
    }

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
        'infodienste_check',
        'setup',
        '@import "EXT:infodienste_check/Configuration/TypoScript/setup.typoscript"'
    );

})();
