<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 't3infodienste Check',
    'description' => 'Mit dieser Extension werden die Serveranforderungen Ihres TYPO3 Systems für den Einsatz der Infodienste Extension "t3infodienste" geprüft. Hierfür wird ein Backendmodul bereitgestellt.',
    'category' => 'module',
    'author' => 'Lukas Kleist',
    'author_email' => 'l.kleist@nerdost.net',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.3',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
