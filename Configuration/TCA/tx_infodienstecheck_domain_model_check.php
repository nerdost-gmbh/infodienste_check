<?php

use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;

$versionInformation = GeneralUtility::makeInstance(Typo3Version::class);

$tcaSetup = [
    'ctrl' => [
        'title' => 'LLL:EXT:infodienste_check/Resources/Private/Language/locallang_db.xlf:tx_infodienstecheck_domain_model_check',
        'label' => 'uid',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'readOnly' => true,
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => '',
        'iconfile' => 'EXT:infodienste_check/Resources/Public/Icons/Extension.svg',
    ],
    'types' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [ ],
            ],
        ],
    ],
];

if ($versionInformation->getMajorVersion() < 12) {
    $tcaSetup['ctrl']['cruser_id'] = 'cruser_id';
    $tcaSetup['columns']['hidden']['config']['items'] = [
        [
            0 => '',
            1 => '',
            'invertStateDisplay' => true
        ]
    ];
} else {
    $tcaSetup['ctrl']['security'] = [
        'ignorePageTypeRestriction' => true,
    ];
    $tcaSetup['columns']['hidden']['config']['items'] = [
        [
            'label' => '',
            'invertStateDisplay' => true
        ]
    ];
}

return $tcaSetup;
