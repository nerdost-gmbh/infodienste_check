<?php

use NERDOST\InfodiensteCheck\Controller\CheckController;

return [
    'infocheck' => [
        'parent' => 'tools',
        'position' => 'bottom',
        'access' => 'admin',
        'workspaces' => 'live',
        'path' => '/module/tools/infocheck',
        'icon' => 'EXT:infodienste_check/Resources/Public/Icons/t3i.svg',
        'labels' => 'LLL:EXT:infodienste_check/Resources/Private/Language/locallang_infocheck.xlf',
        'extensionName' => 'InfodiensteCheck',
        'controllerActions' => [
            CheckController::class => ['new','create','list','index'],
        ],
    ],
];
